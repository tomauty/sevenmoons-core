import test from 'tape';
import Ability from '../classes/Ability.js';

test('Ability construction', (assert) => {
	const a = new Ability(1, 'test', 'spell', {});
	assert.equal(a.get().name, 'test', 'Construction succeeds');
	assert.end()
});
