import test from 'tape';
import Area from '../classes/Area';

test('Area construction', (assert) => {

	const a = new Area(1, "Swamp");
	assert.equals(a.name, "Swamp");
	assert.end();

});
