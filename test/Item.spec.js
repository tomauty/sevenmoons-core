import test from 'tape';
import Item from '../classes/Item';

test('Item construction', (assert) => {
	const i = new Item(1);
	assert.equal(i.getId(), 1);
	assert.equal(i.getRarity(), 0, 'Default rarity of 0');
	assert.end();
});

test('maxStack', (assert) => {
	const i = new Item(1);
	assert.equal(i.getMaxStack(), 99, 'Default stack size of 99');
	i.setMaxStack(-1);
	assert.equal(i.getMaxStack(), 1, 'Can\'t set below 0');
	i.setMaxStack(200);
	assert.equal(i.getMaxStack(), 99, 'Can\'t set above 99');
	assert.end()
});

test('craftability', (assert) => {
	const i = new Item(i);
	assert.equal(i.getCraftable(), false, 'Not initially craftable');
	i.setCraftable(true);
	assert.equal(i.getCraftable(), true, 'Craftability set');
	assert.end()
});

