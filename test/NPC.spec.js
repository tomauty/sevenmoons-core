import test from 'tape';
import NPC from '../classes/NPC.js';
import Entity from '../classes/Entity.js';

test('NPC Construction', (assert) => {

	const fellow = new NPC(0, 'John');
	assert.equal(fellow.name, 'John');
	assert.equal(fellow instanceof Entity, true, 'NPC inherits from Entity');

	assert.equal(fellow.getHP(), Infinity, 'Inherits internal properties from entity');
	assert.end();

});

test('invincibility', (assert) => {

	const fellow = new NPC(0, 'John');
	fellow.decrementHP(100000000);
	assert.equal(fellow.getHP(), Infinity, 'You can\'t kill an NPC');
	assert.end();

});
