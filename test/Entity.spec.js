import test from 'tape';
import Entity from '../classes/Entity';

test('Entity construction', (assert) => {

	const e = new Entity(1, 'Tim');
	assert.equals(e.name, 'Tim', 'Name set in construction');
	assert.end();
});


test('Entity extension prevention', (assert) => {

	const e = new Entity(1, 'Tom');
	const setSomething = () => {
		e.extraProperty = true;
	};

	assert.throws(setSomething);
	assert.end();
});

test('Decrement HP / Chainability', (assert) => {

	const e = new Entity(1, 'Tim', 'any', 100);

	e.decrementHP(10).decrementHP(10);
	assert.equal(e.getHP(), 80, 'Entity methods are chainable');

	e.decrementHP(1.5);
	assert.equal(e.getHP(), 78.5, 'Takes floating point');

	e.decrementHP(200);
	assert.equal(e.getHP(), 0, 'Can\'t go below 0');
	assert.equal(e.getDeath(), true, 'Entity dies when HP hits 0');

	assert.end();
});

test('Increment HP / Chainability', (assert) => {

	const e = new Entity(1, 'Tim', 'any', 100);

	e.incrementHP(1000);
	assert.equal(e.getHP(), 100, 'Cannot heal above max HP');

	e.decrementHP(50).incrementHP(10);
	assert.equal(e.getHP(), 60, 'Can increment');

	e.decrementHP(2000);
	e.incrementHP(100);
	assert.equal(e.getHP(), 0, 'Cant revive without explicit revival');

	assert.end();
});

test('Hostility', (assert) => {
	const e = new Entity(1, 'Test');
	assert.equal(e.getHostility(), false, 'Not hostile by default');
	e.setHostility(true);
	assert.equal(e.getHostility(), true, 'Hostility set');
	assert.end()
});

test('Invincibility', (assert) => {
	const e = new Entity(1, 'Test');
	assert.equal(e.getInvincibility(), false, 'Not invincible by default');
	e.setInvincibility(true);
	assert.equal(e.getInvincibility(), true, 'Invincibility set');
	assert.end()
});
