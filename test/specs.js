require('babel/register');
require('./Ability.spec');
require('./Area.spec');
require('./Entity.spec');
require('./Item.spec');
require('./Inventory.spec');
require('./NPC.spec');
require('./util.spec');
