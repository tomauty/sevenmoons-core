import test from 'tape';
import NPC from '../classes/NPC';
import Entity from '../classes/Entity';
import util from '../util';

test('isNPC', (assert) => {

	const n = new NPC();
	const e = new Entity();

	assert.isEqual(util.isNPC(e), false);
	assert.isEqual(util.isNPC(n), true);

	assert.end();

});

test('verifyProperlyConstructed', (assert) => {

	const thrower = () => {
		util.verifyProperlyConstructed('className', ['number', 'string', Array, Entity], [1, 'something', [], null]);
	};

	assert.throws(thrower);
	assert.end();

});

