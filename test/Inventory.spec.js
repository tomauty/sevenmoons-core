import test from 'tape';
import Inventory from '../classes/Inventory';
import Entity from '../classes/Entity';
import Item from '../classes/Item';

const e = new Entity();

test('Inventory construction', (assert) => {
	const i = new Inventory(e);
	assert.equal(i.getCapacity(), 10);
	assert.end();
});

test('Adding to inventory', (assert) => {
	const i = new Inventory(e, undefined, 2);
	const item1 = new Item(1);
	const item2 = new Item(2);
	const item3 = new Item(3);

	i.addItem(item1).addItem(item1);
	assert.equal(i.getItems().length, 1, 'Multiple of the same items will stack');

	i.addItem(item2);
	assert.equal(i.getItems().length, 2, 'Add a new item');

	const failure = i.addItem(item3);
	assert.equal(failure, false, 'Can\'t add when over capacity');

	assert.end();
});

test('isFull', (assert) => {
	const i = new Inventory(e, undefined, 1);
	const item1 = new Item(1);
	assert.equal(i.isFull(), false, 'New inventory is not full');
	i.addItem(item1);
	assert.equal(i.isFull(), true, 'Hit capacity of 1');

	assert.end()
});
