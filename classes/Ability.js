const _internal = new WeakMap();

class Ability {
	/**
	 * @constructs Ability
	 * @classdesc An ability is something an Entity can do (apart from an Action)
	 *
	 * @param {number} id - Ability ID
	 * @param {string} name - Ability name
	 * @param {string} type - Ability type
	 * @param {object} stats - Ability statsTable
	 *
	 * @returns {Ability} - instance of Ability
	 */
	constructor(id, name, type, stats) {
		_internal.set(this, {
			id,
			name,
			type,
			stats
		});
	}

	get() {
		return _internal.get(this);
	}
}

module.exports = Ability;