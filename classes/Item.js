import _ from 'lodash';
const _internal = new WeakMap();

//const rarities = {
//	0: 'Junk',
//	1: 'Normal',
//	2: 'Nice',
//	3: 'Rare',
//	4: 'Legendary'
//};

class Item {
	/**
	 * @constructs Item
	 * @classdesc An item is something that an Entity can possess, or it can exist in the mists. Who knows, maybe it could fall out of the sky?
	 *
	 * @param {number} id - The ID of the item
	 * @param {string} name - The name of the item
	 * @param {string} type - The type of item
	 * @param {Stats} stats - A stats object, will vary based on type
	 *
	 * @returns {Item} - instance of Item
	 */
	constructor(id, name = 'Dust', type = 'junk', stats = {}) {
		_internal.set(this, {
			id,
			name,
			type,
			stats,
			count: 1,
			isEquipable: false,
			hasUse: false,
			rarity: 0,
			maxStack: 99,
			isCraftable: false
		});

	}

	/**
	 * @method Item#getId
	 * @returns {number} id - The Item's ID
	 */
	getId() {
		return _internal.get(this).id;
	}

	/**
	 * Get the count of items in instance
	 * @method Item#getCount
	 * @returns {number} - The amount of items represented in the instance
	 */
	getCount() {
		return _internal.get(this).count;
	}

	/**
	 * Set the count of items represented by instance
	 * @method Item#setCount
	 * @param {number} amount - The amount of items to be represented
	 * @returns {Item} - The item instance
	 */
	setCount(amount) {
		_internal.get(this).count = amount;
		return this;
	}

	/**
	 * @method Item#getRarity
	 * @returns {number} rarity - The item's rarity (0-4)
	 */
	getRarity() {
		return _internal.get(this).rarity;
	}

	/**
	 * @method Item#getMaxStack
	 * @returns {number} - The maximum stack size for item
	 */
	getMaxStack() {
		return _internal.get(this).maxStack;
	}

	/**
	 * Set the item's max stack size
	 * @method Item#setMaxStack
	 * @param {number} amount - The maximum stack size
	 * @returns {Item} - The item instance
	 */
	setMaxStack(amount) {
		if (!_.isNumber(amount)) { return this; }

		let stackSize = Math.floor(amount);

		stackSize = stackSize > 0
			? stackSize > 99 ? 99 : 0
			: 1;

		_internal.get(this).maxStack = Math.floor(stackSize);
		return this;
	}

	/**
	 * @method Item#getCraftable
	 * @returns {boolean} isCraftable - The item's isCraftable value
	 */
	getCraftable() {
		return _internal.get(this).isCraftable;
	}

	/**
	 * Set craftability of an item
	 * @method Item#setCraftable
	 * @param {boolean} bool - Craftability toggle
	 * @returns {Item} - The item instance
	 */
	setCraftable(bool) {
		_internal.get(this).isCraftable = Boolean(bool);
		return this;
	}
}

module.exports = Item;