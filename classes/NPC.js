import Entity from './Entity.js';
const _internal = new WeakMap();

class NPC extends Entity {

	/**
	 * @constructs NPC
	 * @classdesc A non-playable character that is non-hostile and non-killable
	 *
	 * @augments Entity
	 *
	 * @param {number} id - Entity ID
	 * @param {string} name - Entity name
	 *
	 * @returns {NPC} - NPC Instance
	 */
	constructor(id, name) {
		super(id, name, 'npc', Infinity);
		_internal.set(this, {
			isInvincible: true,
			isHostile: false
		});
	}
}

module.exports = NPC;