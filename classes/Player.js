import Entity from './Entity.js';
import Inventory from './Inventory.js';
const _internal = new WeakMap();

class Player extends Entity {

	/**
	 * @constructs Player
	 * @classdesc A player in Seven Moons
	 *
	 * @augments Entity
	 *
	 * @param {number} id - Player ID
	 * @param {string} name - Player name
	 * @param {number} hp - Player HP
	 *
	 * @returns {Player} - Player instance
	 */
	constructor(id, name, hp) {
		super(id, name, 'player', hp);
		_internal.set(this, {
			isInvincible: false,
			isHostile: false,
			inventory: new Inventory()
		})
	}
}

module.exports = Player;