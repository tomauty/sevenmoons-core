import _ from 'lodash';

class Area {
	/**
	 * @constructs Area
	 * @classdesc A section of the map with a defined type and behavior based on type.
	 *
	 * @param {number} id - The ID of the area.
	 * @param {string} name - The name of the area.
	 * @param {string} type - The type of area that it is.
	 *
	 * @returns {Area} - instance of an Area
	 *
	 */
	constructor(id, name, type) {
		_.extend(this, { id, name, type });
	}
}

module.exports = Area;
