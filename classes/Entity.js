import events from 'events';
import _ from 'lodash';

const _internal = new WeakMap();

class Entity extends events.EventEmitter {
	/**
	 * @constructs Entity
	 * @classdesc An entity is a basic 'being' in the world
	 *
	 * @param {number} id - The ID of the entity`
	 * @param {string} name - The entity's name
	 * @param {string} type - The type of entity
	 * @param {number} hp - The entity's hp and max HP
	 *
	 * @returns {Entity} - instance of Entity
	 */
	constructor(id, name, type, hp) {
		super();
		_.extend(this, {id, name, type });
		_internal.set(this, {
			isInvincible: false,
			isDead: false,
			isHostile: false,
			hp,
			maxHP: hp
		});

		if(this.constructor === Entity) {
			Reflect.preventExtensions(this);
		}
	}

	/**
	 * Get entity HP
	 * @method Entity#getHP
	 * @returns {number} - Entity's HP
	 */
	getHP() {
		return _internal.get(this).hp;
	}

	/**
	 * Set entity HP, to be used internally
	 * @method Entity#setHP
	 * @param {number} amount - Amount to set HP to
	 * @return {Entity} - Entity instance
	 */
	setHP(amount) {
		if (!_.isNumber(amount)) { return this; }
		_internal.get(this).hp = amount;
		if (amount === 0) {
			this.setDeath(true);
		}
		return this;
	}

	/**
	 * Get Entity's max HP
	 * @method Entity#getMaxHP
	 * @returns {number} - Entity's max HP
	 */
	getMaxHP() {
		return _internal.get(this).maxHP;
	}

	/**
	 * Decrement the HP of an entity by the specified amount
	 * @method Entity#decrementHP
	 * @param {number} amount - The amount by which to decrement this entity's HP
	 * @returns {Entity} - The Entity instance
	 */
	decrementHP(amount) {
		if (_internal.get(this).isInvincible || _internal.get(this).isDead) { return this; }
		let newHP = this.getHP() - amount;
		if (this.getHP() <= amount) { newHP = 0; }
		this.setHP(newHP);
		return this;
	}

	/**
	 * Increment the HP of an entity by the specified amount
	 * @method Entity#incrementHP
	 * @param {number} amount - The amount by which to increment
	 * @returns {Entity} - The Entity instance
	 */
	incrementHP(amount) {
		if (_internal.get(this).isInvincible || _internal.get(this).isDead) { return this; }
		let newHP = this.getHP() + amount;
		if (newHP > this.getMaxHP()) {
			newHP = this.getMaxHP();
		}
		this.setHP(newHP);
		return this;
	}

	/**
	 * @method Entity#getDeath
	 * @returns {boolean} - Death status
	 */
	getDeath() {
		return _internal.get(this).isDead;
	}

	/**
	 * Set death status on Entity
	 * @method Entity#setDeath
	 * @param {boolean} death - Death status
	 * @returns {Entity} - Entity instance
	 */
	setDeath(death) {
		if (_internal.get(this).isInvincible) { return this; }
		_internal.get(this).isDead = Boolean(death);
		return this;
	}

	/**
	 * Get hostility
	 * @method Entity#getHostility
	 * @returns {boolean} - Hostility of Entity
	 */
	getHostility() {
		return _internal.get(this).isHostile;
	}

	/**
	 * Sets entity hostility in relation to current player
	 * @method Entity#setHostility
	 * @param {boolean} hostile - Hostility value
	 * @returns {Entity} - Entity instance
	 */
	setHostility(hostile) {
		_internal.get(this).isHostile = Boolean(hostile);
		return this;
	}

	/**
	 * Get invincibility
	 * @method Entity#getInvincibility
	 * @returns {boolean} - Invincibility of Entity
	 */
	getInvincibility() {
		return _internal.get(this).isInvincible;
	}

	/**
	 * Sets entity invincibility in relation to current player
	 * @method Entity#setInvincibility
	 * @param {boolean} invincible - Invincibility value
	 * @returns {Entity} - Entity instance
	 */
	setInvincibility(invincible) {
		_internal.get(this).isInvincible = Boolean(invincible);
		return this;
	}

}

module.exports = Entity;
