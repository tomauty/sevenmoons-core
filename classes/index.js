import Area from './Area';
import Entity from './Entity';
import Item from './Item';
import Map from './Map';
import NPC from './NPC';
import World from './World';

module.exports = {
	Area,
	Entity,
	Item,
	Map,
	NPC,
	World
};
