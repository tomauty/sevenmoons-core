import zmq from 'zmq';
const socket = zmq.socket('pull');

socket.connect('tcp://127.0.0.1:3000');
console.log('Listening to zMQ on port 3000');

socket.on('connect', (msg) => {
	console.log(`[CONNECT]: ${msg}`);
});

socket.on('message', (msg) => {
	console.log(`[MESSAGE]: ${msg}`);
});


const testsend = zmq.socket('push');
setTimeout(function() {
	testsend.bindSync('tcp://127.0.0.1:3000');
	testsend.send('hello!');
}, 2000);

