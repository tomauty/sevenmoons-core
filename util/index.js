/**
 * Util Module
 * @module util
 */
import NPC from '../classes/NPC';
import Player from '../classes/Player';

/**
 * Detect if an Entity is an NPC
 * @function
 * @param {Entity} entity - The entity to test upon
 *
 * @returns {boolean} - Boolean
 */
module.exports.isNPC = (entity) => { return entity instanceof NPC; };

/**
 * Detect if an Entity is a Player
 * @function
 * @param {Entity} entity - The entity to test upon
 *
 * @returns {boolean} - Boolean
 */
module.exports.isPlayer = (entity) => { return entity instanceof Player; };


/**
 * Handle errors for improperly-constructed classes
 *
 * @function
 * @throws Will throw error if class any types don't match
 * @param {string} className - The class name, for error reporting purposes
 * @param {array} typesArray - Array of class literals or type
 * @param {array} argumentArray - Arguments object from constructor
 *
 * @returns {*} nothing
 *
 */
module.exports.verifyProperlyConstructed = (className, typesArray, argumentArray) => {
	_.map(typesArray, (type, index) => {
		const typeOf = typeof argumentArray[index] === type;
		const instanceOf = argumentArray[index] instanceof type;
		if (!typeOf && !instanceOf) {
			throw new Error(`${className} improperly constructed.`);
		}
	});
};